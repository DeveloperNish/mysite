<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        {{ Html::style('css/login.css') }}
    </head>
    <body>
        <main class="login">
            <div class="box">
                <h1>LOGIN</h1>
                <div class="inner-box">
                    {{ Form::open() }}
                        {{ Form::label('username', 'Username') }}
                            {{ Form::text('text', null, array('id'=>'username', 'name'=>'username', 'placeholder'=>'Your username', 'autocomplete'=>'off')) }}
                            <!-- <input type="text" name="username" id="username" placeholder="Your username" autocomplete="off"> -->
                        {{ Form::label('password', 'Password') }}
                            {{ Form::password('password', array('id'=>'password', 'name'=>'password', 'placeholder'=>'Your password')) }}
                            <!-- <input type="password" name="password" id="password" placeholder="Secret password"> -->
                        {{ Form::submit('Login') }}
                    {{ Form::close() }}
                </div>
            </div>
        </main>
    </body>
</html>