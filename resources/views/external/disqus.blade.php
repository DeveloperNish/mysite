<div id="disqus_thread"></div>
@section('bottom-scripts')
<script>
var disqus_config = function () {
this.page.url = "{{Request::url()}}";
this.page.identifier = "{{$article->title}}";
};
var disqus_developer = 1;
(function() { // DON'T EDIT BELOW THIS LINE 
var d = document, s = d.createElement('script');

s.src = '//developernish.disqus.com/embed.js';

s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
@endsection