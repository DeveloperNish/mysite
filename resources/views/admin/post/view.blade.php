@extends('public.master.layout')
@section('title')
{{$article->title}} - DeveloperNish  
@endsection

@section('description')
{{$article->excerpt}}
@endsection

@section('top-scripts')
	<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/7.3/highlight.min.js"></script>
	<script>hljs.initHighlightingOnLoad();</script>
@endsection

@section('styles')
	{{ Html::style('css/themes/tomorrow-night-eighties.css') }}
@endsection

@section('main-content')
<article class="single-post">
    <header class="single-post-header">
        <h1 class="single-post-title">{{ $article->title }}</h1>
        <div class="single-post-meta">
            <ul class="metas">
                <li class="date"><a href="#">{{ $article->publish_date_stamp }}</a></li>
                <li class="tag"><a href="{{ url('/tags/'.$article->tag->name) }}">{{ $article->tag->name }}</a></li>
            </ul>
        </div>
        <p class="single-post-excerpt">
        {{ $article->excerpt }}
        </p>
    </header>
    <div class="single-post-image">
        <img src="/uploads/image/{{ $article->display_image }}" alt="{{ $article->title }}" title="{{ $article->title }}">
    </div>
    <div class="single-post-body">
    	{!! $article->parsed_content !!}
    </div>
</article>

    
@endsection