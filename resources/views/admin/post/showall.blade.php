@extends('admin.master.layout')


@section('main-content')
<h2>All Articles</h2>
<div class="search">
	<input type="text" id="search" name="search">
	<label for="search">Search</label>
	<ul class="search-results">
		<li class="result-item"><a href="#">result</a></li>
		<li class="result-item"><a href="#">Another item</a></li>
		<li class="result-item"><a href="#">Another item</a></li>
		<li class="result-item"><a href="#">Another item</a></li>
		<li class="result-item"><a href="#">Another item</a></li>
	</ul>
</div>

@if(isset($articles))
    @if ($articles->count())
    	<?php $i = 0; ?>
        @foreach ($articles as $article)
            @if ($i%2 == 0)
                <div class="post-row">
            @endif
            	<div class="post">
            		<div class="post-date">
            			<p>{{$article->publish_date_carbon->day}}</p>
            			<p>{{$article->getShortPublishedMonth()}}</p>
            			<p>{{$article->publish_date_carbon->year}}</p>
            		</div>
            		<div class="post-content">
            			<h3>{{$article->title}}</h3>
            			<p>{{$article->excerpt}}</p>
            		</div>
            		<div class="post-controls">
            			<a href="{{url('/post/'.$article->getUrlSafeTitle())}}">View</a>
						<a href="{{url('/admin/post/'.$article->getUrlSafeTitle().'/edit')}}">Edit</a>
						<a href="{{url('/admin/post/'.$article->getUrlSafeTitle().'/delete')}}">Delete</a>
					</div>
            	</div>
            @if ($i%2 != 0 || $articles->count()==1)
                </div>
            @endif
            <?php $i++ ?>
        @endforeach
    @else
        <h1 align="center" style="opacity:0.5; font-weight: 600;">No Posts to Display</h1>
    @endif
    @if($articles->hasPages())
        <div align="center">
        {{ $articles->render() }}
        </div>
    @endif
@endif

@section('bottom-scripts')
<script type="text/javascript">
var search_input = $("#search");
var search_results = $(".search-results");
var base_url = "http://"+window.location.host;
var search_url = base_url+"/admin/post/search";
var xcsrf = $("meta[name=X-CSRF-TOKEN]").attr("content");
var prev = 0;
search_input.keyup(function(e){
	if(search_input.val().length != 0)
	{
		$.ajax({
			url : search_url,
			type : "post",
			data : {'search':search_input.val(),'_token':xcsrf},
			success : function(data){
				if(data.length != 0)
				{
					var html = "";
					for(var i=0;i<data.length;i++){
						var title = data[i].title;
						html += '<li class="result-item"><a href="'+base_url+'/admin/post/'+title.replace(/\s+/g, '-').toLowerCase()+'/edit">'+title+"</a></li>";
					}
					search_results.html("");
					search_results.html(html);
					search_results.slideDown();
				}else{
					search_results.slideUp();
				}
				
			}
		});
			
	}else{
		search_results.slideUp();
	}
});
</script>
@endsection

@endsection