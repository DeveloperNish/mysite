@extends("admin.master.layout")

@section('styles')
{{Html::style('css/libs/dropzone.css')}}
@endsections

@section('top-scripts')
{{Html::script('js/libs/autosize.min.js')}}
{{Html::script('js/libs/dropzone.js')}}
@endsection

@section('main-content')
{!!Form::open(array('route'=>'admin.post.create','name'=>'dataForm'))!!}
<h2>Create a new post</h2>
<div class="form-group">
{{Form::label('title','Title')}}
{{Form::text('title',old('name'))}}
@if($errors->has('title'))
		<p class="error">{{$errors->first('title')}}</p>
@endif
</div>
<div class="form-group">
{{Form::label('excerpt','Excerpt')}}
{{Form::text('excerpt')}}
@if($errors->has('excerpt'))
	<p class="error">{{$errors->first('excerpt')}}</p>
@endif
</div>
<div class="form-group">
{{Form::label('publish_date','Publish Date')}}
{{Form::date('publish_date',Carbon\Carbon::now())}}
@if($errors->has('publish_date'))
	<p class="error">{{$errors->first('publish_date')}}</p>
@endif	
</div>
<div class="form-group">
{{Form::label('tag_id','Category')}}
<select id="tag_id" name="tag_id">
	@foreach ($tags as $tag)
		<option value="{{$tag->id}}">{{$tag->name}}</option>
	@endforeach
</select>
@if($errors->has('category'))
	<p class="error">{{$errors->first('tag_id')}}</p>
@endif
</div>
<div class="form-group">
{{Form::label('display_image','Display Image')}}
{{Form::number('display_image')}}
@if($errors->has('display_image'))
	<p class="error">{{$errors->first('display_image')}}</p>
@endif
</div>
<div class="form-group">
{{Form::label('content','Content')}}
@include('admin.partials.content-menu')
{{Form::textarea('content')}}
@if($errors->has('content'))
	<p class="error">{{$errors->first('content')}}</p>
@endif
</div>
<div class="form-group">
{{Form::button('View',['class'=>'view'])}}
{{Form::submit('Create')}}
</div>
{!!Form::close()!!}
<div id="overlay">
</div>
<div class="image-box">
	<div class="image-box-header">
		<h2 class="image-box-header-title">
			Image Picker
		</h2>
		<span class="image-box-header-close">
			<i class="image-box-header-close-button fa fa-times"></i>
		</span>
	</div>
	<div class="image-box-tabs">
		<span class="image-box-tab image-box-selected">Pictures</span>
		<span class="image-box-tab">Upload</span>
	</div>
	<div class="pictures">

	</div>
	<div class="upload" id="upload">
		{{Form::open(['route'=>'admin.image.upload','class'=>'dropzone','id'=>'upload-file'])}}
		<div class="fallback">
		    <input name="file" type="file" multiple />
		</div>
		{{Form::close()}}
	</div>
</div>
@endsection

@section('bottom-scripts')
{{Html::script('js/view-handler.js')}}
{{Html::script('js/sticky-content-menu.js')}}
<script>
function _insertAtCaret(element, text) {
    var caretPos = element[0].selectionStart,
        currentValue = element.val();

    element.val(currentValue.substring(0, caretPos) + text + currentValue.substring(caretPos));
  document.getElementsByName("content")[0].selectionStart = caretPos + text.length;
  document.getElementsByName("content")[0].selectionEnd = caretPos + text.length;
}
var dataTypes = {
	h1 		: 	"\n#Heading One",
	h2 		: 	"\n##Heading Two",
	h3 		: 	"\n###Heading Three",
	h4 		: 	"\n####Heading Four",
	h5 		: 	"\n#####Heading Five",
	h6 		: 	"\n######Heading Six",
	b 		: 	"**bold text here**",
	i 		: 	"*italic text here*",
	u   	: 	"<u>underlined text here</u>",
	img 	: 	' ![alternate text](http://website.com "Image title text") ',
	a 		: 	' [link text](http://example.com/ "title text") ',
	code 	: 	'\n```\n//code goes here\n```',
	olist 	: 	'\n1. item one \n2. item two \n3. item 3',
	ulist 	: 	'\n* item one \n* item two \n* item 3',
	sep 	: 	'\n_________________________\n',
};
$('.content-menu-item').click(function(){

	var sel = $(this).attr('data-type');
	if(sel in dataTypes){
		if(sel==="img"){
			$('#overlay').fadeIn();
			$('.image-box').fadeIn();
			getAllImages(sel);
		}else{
			_insertAtCaret($('textarea[name=content]'),dataTypes[sel]);
		}
	}
});


var overlay = $('#overlay');
var imagebox = $('.image-box');
imagebox.hide();
overlay.hide();
overlay.click(function(){
	$(this).fadeOut();
	imagebox.fadeOut();
});
//onclick of imagebox close button
$('.image-box-header-close-button').click(function(){
	imagebox.fadeOut();
	overlay.fadeOut();
});
//tabs switching
$('.image-box-tab').click(function(){
	$('.image-box-tab').removeClass('image-box-selected');
	$(this).addClass('image-box-selected');
	if($(this).html()=="Pictures"){
		$('.pictures').show();
		$('.upload').hide();
	}else if($(this).html()=="Upload"){
		$('.pictures').hide();
		$('.upload').show();
	}
});

//Onclick of display image input box
$('#display_image').click(function(){
	overlay.fadeIn();
	imagebox.fadeIn();
	getAllImages("disp");
});


function getAllImages(to)
{
	var base_url = "http://"+window.location.host;
	var all_image_url = base_url+"/uploads/image/all";
	var xcsrf = $("meta[name=X-CSRF-TOKEN]").attr("content");
	$.ajax({
		url : all_image_url,
		type : "post",
		data : {'_token':xcsrf},
		success : function(data) {
			if(data.length != 0) 
			{
				var html = "";
				$.each(data,function(index,value){
					html += "<span class='box'><img class='box-bg' data-id='"+value.id+"'' onclick='getImg(this,\""+to+"\")' src='"+base_url+"/uploads/image/"+value.id+"'></span>";
				});
				$('.pictures').html(html);
			}
		}
	});
}

function getImg(from,to)
{
	if(to == "disp") {
		var id = $(from).attr('data-id');
		$("#display_image").val(id);
	}else if(to == "img"){
		var src = $(from).attr('src');
		_insertAtCaret($('textarea[name=content]'),"![alternate text]("+src+" \"Image title text\")");
	}
	imagebox.fadeOut();
	overlay.fadeOut();
}
Dropzone.options.uploadFile = {
    paramName: "file", // The name that will be used to transfer the file
    maxFilesize: 3, // MB
    acceptedFiles:"image/*",	
};
</script>
@endsection
