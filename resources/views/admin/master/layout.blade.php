<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Admin Panel</title>
        <meta name="description" content="Admin panel">
        <meta name="viewport" content="width=device-width">
        <meta name="X-CSRF-TOKEN" content="{{csrf_token()}}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        {{ Html::style('css/libs/bs-pagination.css') }}
        {{ Html::style('css/admin.css') }}
        @yield('styles')
        <link rel="icon" type="image/png" href="{{ url('images/logo.png') }}" />
        {{Html::script('js/libs/jquery.min.js')}}
        @yield('top-scripts')
    </head>
    <body>
        @include("admin.partials.fixed-left-nav")
        @include("admin.partials.fixed-top-header")

        <div class="responsive-right">
            @yield("main-content")
        </div>
        @yield('bottom-scripts')
        {{Html::script('js/dropdown-handler.js')}}
        <script type="text/javascript">
            /** 
             * Font Render Fix on Chrome
             */
            var body = $('body');
            body.hide(); body.show();
        </script>
    </body>
</html>
