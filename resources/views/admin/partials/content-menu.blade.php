<ul class="content-menu">
	<li class="content-menu-item" data-type="h1" title="Header 1">H1</li>
	<li class="content-menu-item" data-type="h2" title="Header 2">H2</li>
	<li class="content-menu-item" data-type="h3" title="Header 3">H3</li>
	<li class="content-menu-item" data-type="h4" title="Header 4">H4</li>
	<li class="content-menu-item" data-type="h5" title="Header 5">H5</li>
	<li class="content-menu-item" data-type="h6" title="Header 6">H6</li>
	<li class="content-menu-separator"></li>
	<li class="content-menu-item" data-type="b" title="BOLD"><i class="fa fa-bold"></i></li>
	<li class="content-menu-item" data-type="i" title="ITALICS"><i class="fa fa-italic"></i></li>
	<li class="content-menu-item" data-type="u" title="Underline"><i class="fa fa-underline"></i></li>
	<li class="content-menu-separator"></li>
	<li class="content-menu-item" data-type="img" title="Insert Image"><i class="fa fa-picture-o"></i></li>
	<li class="content-menu-item" data-type="a" title="Anchor link"><i class="fa fa-link"></i></li>
	<li class="content-menu-separator"></li>
	<li class="content-menu-item" data-type="code" title="Insert Code"><i class="fa fa-code"></i></li>
	<li class="content-menu-item" data-type="olist" title="Insert Ordered List"><i class="fa fa-list-ol"></i></li>
	<li class="content-menu-item" data-type="ulist" title="Insert UnOrdered List"><i class="fa fa-list-ul"></i></li>
	<li class="content-menu-separator"></li>
	<li class="content-menu-item" data-type="sep" title="Insert Line Separator"><i class="fa fa-minus"></i>
</li>
</ul>