<header class="fixed-top">
    <a href="#"><div class="logo">
        <img src="{{ url('images/dash-logo-v2.png') }}">
    </div></a>
    <div class="drop-menu">
        <div class="drop-menu-title">
            Nischal Shrestha
        </div>
        <div class="drop-menu-items">
            <a href="#" class="drop-menu-item user">Profile</a>
            <a href="#" class="drop-menu-item settings">Settings</a>
            <a href="{{ url('admin/logout') }}" class="drop-menu-item logout">Logout</a>
        </div>
    </div>
</header>

@section('bottom-scripts')
<script>
/**
 * trigger handler for drop down menu
 */
var drop_menu = document.getElementsByClassName("drop-menu-title");
var dropped = false;
drop_menu[0].onclick = function(){ 
    var drop_menu_items = document.getElementsByClassName("drop-menu-items");
    if(dropped == false)
    {
        dropped = true;
        drop_menu_items[0].style.display = "block";
    }else{
        dropped = false;
        drop_menu_items[0].style.display = "none";
    }
}
</script>
@endsection