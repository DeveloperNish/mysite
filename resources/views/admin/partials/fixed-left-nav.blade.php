<input type="checkbox" id="hide-left" class="hide-left">
<label for="hide-left"></label>
<div class="fixed-left-menu">
    <nav class="menu-group">
        <div class="menu">
            <a href="{{ route('admin.panel') }}" class="menu-item dashboard
            @if(Request::url()==route('admin.panel')) selected @endif"><span>Dashboard</span></a>
            <a href="{{ route('admin.messages') }}" class="menu-item messages
             @if(Request::url()==route('admin.messages')) selected @endif"><span>Messages</span></a>
        </div>
        <div class="menu">
            <a href="{{ route('admin.post.all') }}" class="menu-item all-posts
             @if(Request::url()==route('admin.post.all')) selected @endif"><span>All Posts</span></a>
            <a href="{{ route('admin.post.create') }}" class="menu-item create-post
             @if(Request::url()==route('admin.post.create')) selected @endif"><span>Create Post</span></a>
        </div>
        <div class="menu">
            <a href="{{ route('admin.user.all') }}" class="menu-item all-users
             @if(Request::url()==route('admin.user.all')) selected @endif"><span>All Users</span></a>
            <a href="{{ route('admin.user.create') }}" class="menu-item add-user
             @if(Request::url()==route('admin.user.create')) selected @endif"><span>Add User</span></a>
        </div>
    </nav>
</div>