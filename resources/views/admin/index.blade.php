@extends('admin.master.layout')


@section('main-content')
<h2 class="page-title">Dashboard</h2>
<!--Summary Start-->
<div class="summary">
    <div class="summary-item">
        <div class="summary-logo blue-bg">
            <i class="fa fa-user"></i>
        </div>
        <div class="right-data">
            <div class="data-type">
                Users
            </div>
            <div class="data-count">
            {{$count['users']}}
            </div>
        </div>
    </div>
    <div class="summary-item">
        <div class="summary-logo red-bg">
            <i class="fa fa-archive"></i>
        </div>
        <div class="right-data">
            <div class="data-type">
                Posts
            </div>
            <div class="data-count">
                {{$count['posts']}}
            </div>
        </div>
    </div>
    <div class="summary-item">
        <div class="summary-logo green-bg">
            <i class="fa fa-envelope"></i>
        </div>
        <div class="right-data">
            <div class="data-type">
                Messages
            </div>
            <div class="data-count">
                {{$count['messages']}}
            </div>
        </div>
    </div>
    <div class="summary-item">
        <div class="summary-logo yellow-bg">
            <i class="fa fa-eye"></i>
        </div>
        <div class="right-data">
            <div class="data-type">
                Monthly Visits
            </div>
            <div class="data-count">
                {{$count['views']}}
            </div>
        </div>
    </div>
</div>
<!--Summary End-->
<div class="stats">
    <div class="visitor-map">
    	<script type="text/javascript" src="//rf.revolvermaps.com/0/0/6.js?i=5f5p2sbtmrm&amp;m=1c&amp;s=340&amp;c=ff0000&amp;cr1=ffffff&amp;f=arial&amp;l=0&amp;bv=60&amp;cw=ffffff&amp;cb=3498db" async="async"></script>
    </div>  
    <div class="visitor-chart">
        
    </div>
</div>
@endsection