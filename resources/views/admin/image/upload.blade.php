@extends("admin.master.layout")

@section('top-scripts')
{{Html::script('js/libs/dropzone.js')}}
@endsection

@section('styles')
{{Html::style('css/libs/dropzone.css')}}
@endsection

@section('main-content')
<h2>Upload Image </h2>
{{Form::open(['class'=>'dropzone','id'=>'upload-file'])}}
<div class="fallback">
    <input name="file" type="file" multiple />
</div>
{{Form::close()}}


@endsection
@section('bottom-scripts')
<script type="text/javascript">	
$(function(){
     Dropzone.options.uploadFile = {
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 3, // MB
        acceptedFiles:"image/*",	
      };
});
 </script>
@endsection