@extends("public.master.layout")
@section('title')
Index Page
@endsection

@section('description')
Developernish.com
@endsection

@section('styles')
{{ Html::style('css/libs/bs-pagination.css') }}
@endsection

@section('main-content')
@include('public.partials.articles')

@endsection