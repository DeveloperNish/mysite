<!DOCTYPE html>
<html lang="en-au">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <title>@yield("title")</title>
        <meta name="description" content="@yield('description')">
        <meta content="True" name="HandheldFriendly">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link rel="icon" type="image/png" href="{{ url('images/logo.png') }}" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        {{ Html::style('css/screen.css') }}
        @yield("styles")
        @yield('top-scripts')
    </head>
    <body>
        @include("public.nav.fixed-left")
        
        <main class="responsive-right">
            <div class="page">
                <header class="nav-top">
                    <ul>
                        <li><a href="#">Top Posts</a></li>
                        <li><a href="#">Recent Posts</a></li>
                    </ul>
                </header>
                <section class="main-content">
                    @yield("main-content")
                </section>
            </div>
        </main>
        <div class="hidden">
            <script type="text/javascript" src="//rf.revolvermaps.com/0/0/6.js?i=5f5p2sbtmrm&amp;m=1c&amp;s=340&amp;c=ff0000&amp;cr1=ffffff&amp;f=arial&amp;l=0&amp;bv=60&amp;cw=ffffff&amp;cb=3498db" async="async"></script>
        </div>
        @yield("bottom-scripts")
    </body>
</html>
