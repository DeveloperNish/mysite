@extends('public.master.layout')

@section('title')
{{$tag->name}} - DeveloperNish
@endsection

@section('description')
All posts under the tag of {{$tag->name}}
@endsection

@section('styles')
{{ Html::style('css/libs/bs-pagination.css') }}
@endsection

@section('main-content')
<header class="title">
    <h1>{{ $tag->name }}</h1>
    <p>Displaying all posts under “{{ $tag->name }}”</p>
</header>

@include('public.partials.articles')
@endsection
