@extends("public.master.layout")
@section('title')
About Me
@endsection

@section('description')
Things you need to know about me
@endsection

@section('styles')
<style type="text/css">
.main-content::before {
	border: 0 !important;
	margin: 0 !important;
}
</style>
@endsection

@section('main-content')
{{--
<header class="title">
	<h2>About Me</h2>
	<p>A something about me and my life</p>
</header>
--}}
<div class="about-me">
	<div class="intro">
		<h3>HI, MY NAME IS NISCHAL SHRESTHA</h3>
		<p>
		I am a recent graduate from University of Technology Sydney. I
		was born in Nepal where i was raised until i moved to Bahrain
		where i continued my studies. I was exposed to the wonderful 
		world of programming languages. Computer and web programming 
		always fascinated me. I have had jumped from various programming 
		languages including c, c++, java and much more but then i settled 
		on proramming for the web and choose to learn and specialize in php 
		and the web developement in general.
		</p>
		{{--
		<h3>THINGS TO KNOW ABOUT ME</h3>
		<p>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur.
		</p>
		--}}
	</div>
</div>

<div class="about-skills">
	<h3>MY SKILLS</h3>
	<div class="p-wrap progress" data-p-percent="90">
		PHP
		<div class="p-bar progress">90%</div>
	</div>
	<div class="p-wrap progress" data-p-percent="80">
		HTML
		<div class="p-bar progress">80%</div>
	</div>
	<div class="p-wrap progress" data-p-percent="65">
		JAVASCRIPT
		<div class="p-bar progress">65%</div>
	</div>
	<div class="p-wrap progress" data-p-percent="75">
		CSS
		<div class="p-bar progress">75%</div>
	</div>
	<div class="p-wrap progress" data-p-percent="70">
		LARAVEL
		<div class="p-bar progress">70%</div>
	</div>

	<a href="{{ url('cv/nischalshrestha-cv.pdf')}}" download="nischalshrestha-cv">DOWNLOAD MY CV</a>
</div>

@endsection

@section('bottom-scripts')
<script>
{{--Credit: https://codepen.io/anon/pen/mAzOvJ --}}
// on page load...
moveProgressBar();
// on browser resize...
$(window).resize(function() {
  moveProgressBar();
});

// SIGNATURE PROGRESS
function moveProgressBar() {
  console.log("moveProgressBar");
  var i;
  var pwrap = $('.p-wrap');
  for(i = 0; i <  pwrap.length; i++) {
  	console.log($(pwrap[i]).data('p-percent'));
	var getPercent = ($(pwrap[i]).data('p-percent') / 100);
	var getProgressWrapWidth = $(pwrap[i]).width();
	var progressTotal = getPercent * getProgressWrapWidth;
	var animationLength = 2000;
	
	// on page load, animate percentage bar to data percentage length
  	// .stop() used to prevent animation queueing
	$(pwrap[i]).find('.p-bar').stop().animate({
    	left: progressTotal
  	}, animationLength);
  }
  
}
</script>
@endsection