@extends("public.master.layout")
@section('title')
Contact Me
@endsection

@section('description')
Leave me a message
@endsection

@section('main-content')
<header class="title">
	<h2>Contact Me</h2>
	<p>Do leave a message, would love to say hi :) <br> <a href="mailto:developer.nish@gmail.com">developer.nish@gmail.com</a></p>
</header>


@endsection