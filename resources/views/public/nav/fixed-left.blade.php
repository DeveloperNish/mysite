<input type="checkbox" id="toggle-menu" class="toggle-menu">
<label for="toggle-menu"></label>
<div id="overlay"></div>
<section class="fixed-left">
    <header class="logo">
        <a href="{{url('/')}}">{{ Html::image("images/logo.png") }}</a>
    </header>
    
    <input type="checkbox" id="burger-menu" class="burger-menu">
    <label for="burger-menu"></label>
            
    <nav class="main-nav">
        <ul>
            @foreach($tags as $t)
            <a href="{{ url('tags/'.$t->name) }}">
                @if(isset($tag))
                    @if($tag->name == $t->name)
                        <li class="active">
                    @else
                        <li>
                    @endif
                @else
                    <li>
                @endif
                {{ $t->name }}</li>
            </a>
            @endforeach
            <a href="{{ url('/about') }}"><li>About</li></a>
            <a href="{{ url('/contact') }}"><li>Contact</li></a>
        </ul>
    </nav>
    
    <footer class="social-nav">
        <ul>
            <a target="_blank" href="https://www.facebook.com/nish.shres"><li><i class="fa fa-facebook-square"></i></li></a>
            <a target="_blank" href="https://github.com/DeveloperNish"><li><i class="fa fa-github-square"></i></li></a>
            <a target="_blank" href="https://www.instagram.com/developernish"><li><i class="fa fa-instagram"></i></li></a>
            <a target="_blank" href="https://www.linkedin.com/in/developer-nish"><li><i class="fa fa-linkedin-square"></i></li></a>
        </ul>
        <p class="copyright">
            developernish.com<br>
            2016-2017 &copy;
        </p>
    </footer>
</section>