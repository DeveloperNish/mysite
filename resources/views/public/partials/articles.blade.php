
@if(isset($articles))
    <?php $i = 0; ?>
    @if ($articles->count())        
            @foreach ($articles as $article)
                @if( $article->publish_date <= \Carbon\Carbon::now())
                    @if ($i%2 == 0)
                        <div class="two-col">
                    @endif
                    <article>
                        <header><a href="{{ url('post/'.$article->getUrlSafeTitle()) }}"><img src="/uploads/image/{{ $article->display_image }}"></a></header>
                        <h2><a href="{{ url('post/'.$article->getUrlSafeTitle()) }}">{{ $article->title }}</a></h2>
                        <p>{{ $article->excerpt }}</p>
                        <footer>
                            <ul class="meta">
                                <li class="date">
                                    <a href="{{ url('post/'.$article->getUrlSafeTitle()) }}">{{ $article->publish_date_stamp }}</a>
                                </li>
                                <li class="tag">
                                    <a href="{{ url('tags/'.$article->tag->name) }}">{{ $article->tag->name }}</a>
                                </li>
                            </ul>
                        </footer>
                    </article>
                    @if ($i%2 != 0 || $articles->count()==1)
                        </div>
                    @endif
                    <?php $i++ ?>
                @endif
            @endforeach
            @if ($i==0)
                <h1 align="center" style="opacity:0.5; font-weight: 600;">No Posts to Display</h1>
            @endif
        @else
            <h1 align="center" style="opacity:0.5; font-weight: 600;">No Posts to Display</h1>
        @endif
        @if($articles->lastPage())
            <div align="center">
            {{ $articles->render() }}
            </div>
        @endif
    @endif