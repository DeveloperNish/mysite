<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public $timestamps = false;

    /**
     * A tag has many articles
     * 1:M
     */
    public function articles()
    {
    	return $this->hasMany('App\Article');
    }
}
