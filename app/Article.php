<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','tag_id','title', 'excerpt', 'display_image', 'content', 'publish_date'
    ];

    protected $dates = ['publish_date','created_at','updated_at'];

    public static $createRules = array(
        'title'         =>  'required|unique:articles|max:255|regex:/^[a-z\d ]+$/i',
        'excerpt'       =>  'required|max:500',
        'content'       =>  'required',
        'publish_date'  =>  'required|date|after:yesterday',
        'display_image' =>  'required',
        'tag_id'        =>  'required'
    );
    public static $editRules = array(
        'title'         =>  'required|max:255|regex:/^[a-z\d ]+$/i',
        'excerpt'       =>  'required|max:500',
        'content'       =>  'required',
        'publish_date'  =>  'required|date',
        'display_image' =>  'required',
        'tag_id'        =>  'required|digits:1'
    );
    
    public static $messages = array(
        'title.unique'          =>  'This title has already been used',
        'title.regex'           =>  'Title can only contain Alphabets, Numbers and Spaces',
        'publish_date.after'    =>  'Article can only be published in current or future dates',
    );

    public function getPublishDateAttribute($value)
    {
    	$date = new Carbon($value);
    	return $date->format('Y-m-d');
    }

    public function getShortPublishedMonth()
    {
        $date = new Carbon($this->publish_date);
        return $date->format('M');
    }

    /*public function getContentAttribute($value)
    {
        $content = \Markdown::parse($value);
        return $content;
    }*/

    public function getParsedContentAttribute()
    {
        $content = \Markdown::parse($this->content);
        return $content;
    }

    public function getPublishDateStampAttribute()
    {
        $date = new Carbon($this->publish_date);
        return $date->format('Y-m-d');
    }

    public function getPublishDateCarbonAttribute()
    {
        $date = new Carbon($this->publish_date);
        return $date;
    }


    public function scopeLatest($query)
    {
        return $query->orderBy('publish_date','desc');
    }


    /**
     * Article Belongs to a User
     * 1:M
     */
    
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    /**
     * Article belongs to a tag
     * 1:M
     */
    
    public function tag()
    {
    	return $this->belongsTo('App\Tag');
    }

    public function getUrlSafeTitle(){ 
        return preg_replace('/\s+/', '-', $this->title);
    }

    private function getUrlUnsafeTitle()
    {
        return preg_replace('/-/',' ',$this->title);
    }

    public static function urlSafeTitle($title)
    {
        return preg_replace('/\s+/', '-', $this->title);
    }

    public static function urlUnsafeTitle($title)
    {
        return preg_replace('/-/',' ',$title);
    }
}
