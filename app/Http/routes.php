<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/





// Registration routes...
// Route::get('auth/register', 'Auth\AuthController@getRegister');
// Route::post('auth/register', 'Auth\AuthController@postRegister');


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'PageController@index');
    Route::get('/about','PageController@about');
    Route::get('/contact','PageController@contact');


    //Post Routes
    Route::get('/post/{post}','ArticleController@show')->where('post','[a-zA-Z0-9-\']+');

    //Tags Routes
    Route::get('/tags/{tag}','TagController@index')->where('tag','[a-zA-Z]+');



    // Admin routes
    // admin.panel
    Route::get('admin/panel', 	['as' => 'admin.panel',
    							'uses'=>'AdminController@index']);
    //admnin.post.all
    Route::get('admin/post/all',	['as' => 'admin.post.all',
    								'uses' => 'AdminController@showAllPost']);

    //admin.post.create
    Route::get('admin/post/create', ['as' => 'admin.post.create',
    								'uses' => 'AdminController@showCreate']);
    Route::post('admin/post/create', ['as' => 'admin.post.create',
    								'uses' => 'AdminController@createPost']);
    
    //admin.post.edit
    Route::get('admin/post/{title}/edit',  ['as'=>'admin.post.edit',
    										'uses'=>'AdminController@showEdit'])
    										->where('title','[a-zA-Z0-9-\']+');
    Route::post('admin/post/{title}/edit',  ['as'=>'admin.post.edit',
    										'uses'=>'AdminController@updatePost'])
    										->where('title','[a-zA-Z0-9-\']+');
    //admin.post.delete
    Route::get('admin/post/{title}/delete', ['as'=>'admin.post.delete',
    										'uses'=>'AdminController@deletePost'])
    										->where('title','[a-zA-Z0-9-\']+');
    //admin.post.search
    Route::post('admin/post/search',['as'=>'admin.post.search',
    										'uses'=>'AdminController@searchPosts'])
    										->where('title','[a-zA-Z0-9-\']+');

    Route::post('admin/post/view/{token}','AdminController@viewPost');
    //admin.post.messages
    Route::get('admin/messages',['as'=>'admin.messages',
    							'uses'=>'AdminController@showMessages']);
    //admin.user.all
    Route::get('admin/user/all',['as'=>'admin.user.all',
    							'uses'=>'AdminController@allUsers']);
    //admin.user.create
    Route::get('admin/user/create',['as'=>'admin.user.create',
    								'uses'=>'AdminController@showCreateUser']);

    Route::get('admin/image/upload',['as'=>'admin.image.upload',
                                     'uses'=>'AdminController@showForm']);

    Route::post('admin/image/upload',['as'=>'admin.image.upload',
                                     'uses'=>'ImageController@uploadImage']);
    Route::post('uploads/image/all',['as'=>'uploads.images.all',
                                     'uses'=>'ImageController@getAllImages']);
    Route::get('uploads/image/{id}',['as'=>'uploads.images',
                                      'uses'=>'ImageController@getImage']);





	// Authentication routes...
	Route::get('admin/login', ['as'=>'admin.login',
								'uses'=>'Auth\AuthController@getLogin']);
	Route::post('admin/login', 'Auth\AuthController@postLogin');
	Route::get('admin/logout', function(){
		if(Auth::check())
		{
			Auth::logout();
		}
		return redirect("admin/login");
	});
});
