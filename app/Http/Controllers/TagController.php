<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Tag;
use App\Article;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index($tag_name)
    {
    	$tag = Tag::where('name','=',$tag_name)->firstOrFail();
		$articles = $tag->articles()->latest()->paginate(10);
    	
    	return view('public.showall',compact('tag','articles'));
    }
}
