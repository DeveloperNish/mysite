<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\ArticlePostRequest;
use App\Image;
use App\Tag;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
	/**
	 * sets how many articles can be shown at once in a page
	 * @var integer
	 */
	private $paginate = 10;

	/**
	 * sets the middleware to "auth"
	 */
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Page showing updated informations
     * @return view [admin/index]
     */
    public function index()
    {
    	$count = array(
    		'posts'	=>	Article::all()->count(),
    		'users'		=>	User::all()->count(),
    		'messages'	=>	'9', //Unread Messages
    		'views'		=>	'32340', //monthly views
    	);
    	return view("admin.index",compact('count'));
    }

    /**
     * Page showing all the users
     * @return view [admin/user/showall]
     */
    public function allUsers()
    {
        $user =  User::all();
        return view("admin.user.showall",compact($user));
    }

    /**
     * Page to create a new user
     * @return view [admin/user/create]
     */
    public function showCreateUser()
    {
        return view("admin.user.create");
    }

    public function showMessages()
    {
        return view("admin.message.showall");
    }

    /**
     * Returns array containing all posts
     * @return view [admin/post/showall]
     */
    public function showAllPost()
    {
    	$articles = Article::latest()->paginate($this->paginate);
    	return view("admin.post.showall", compact('articles'));
    }

    /**
     * Displays a create post form
     * @return view [admin/post/create]
     */
    public function showCreate()
    {
    	//also gets the tags from AppServiceProvider
    	return view("admin.post.create");
    }

    /**
     * Handles Post creation
     * @param  Request $request [Request object to fetch post data]
     * @return view           [admin/post/edit]
     */				
    public function createPost(Request $request)
    {
    	$validator = Validator::make($request->all(), Article::$createRules, Article::$messages);

    	if($validator->fails())
    	{
    		return redirect()->route('admin.post.create')->withErrors($validator)->withInput();
    	}
    	else
    	{
    		$article = new Article;
    		$article->fill($request->all());
			$article->user()->associate(Auth::user());  		
    		$article->save();
    		Session::flash('message','Article has been created at '.$article->created_at->toDateString());
    		return redirect()->route('admin.post.edit',['title'=>$article->getUrlSafeTitle()]);
    	}
    }

    /**
     * Displays edit form to the user with prefilled data
     * @param  String $post [name of the post to edit]
     * @return view       [admin/post/edit]
     */
    public function showEdit($post)
    {
    	$article = $this->getArticleByTitle($post);
    	return view("admin.post.edit",compact('article'));
    }

    /**
     * updates the edited post on request
     * @param  String  $post    [name of the post to edit]
     * @param  Request $request [Request object to fetch post data]
     * @return view           [admin/post/edit]
     */
    public function updatePost($post,Request $request)
    {
		$validator = Validator::make($request->all(), Article::$editRules, Article::$messages);

		if($validator->fails())
		{
			return redirect()->route('admin.post.edit',['title'=>$post])->withErrors($validator)
																		->withInput();
		}
		else
		{
			$article = $this->getArticleByTitle($post);
			$article->fill($request->all());
			$article->save();
			Session::flash('message', 'Post has been updated at '.$article->updated_at->toDateString());
			return redirect()->route('admin.post.edit',['title'=>$article->getUrlSafeTitle()]);
		}

    	dd($request->content);
    }

    /**
     * delete an article
     * @param  String $post [name of the post to delete]
     * @return String
     */
    public function deletePost($post)
    {
    	$article = $this->getArticleByTitle($post);
    	$article->forceDelete();
    	return "Article Deleted!";
    }

    public function searchPosts(Request $request)
    {
    	if($request->ajax())
    	{
    		$articles = Article::where("title","LIKE","%$request->search%")->get();
			return response()->json($articles);
    	}
    }

    public function viewPost(Request $request,$token)
    {
    	if($request->_token != $token){
    		return redirect(403);
    	}else{
    		$article = new Article($request->all());
    		return view('admin.post.view',compact('article'));
    	}
    }

    /**
     * Returns article with the given title
     * @param  String $post [name of the article to fetch]
     * @return Article       [an Article object]
     */
    protected function getArticleByTitle($post)
    {
    	return Article::with('tag')->where('title','=',Article::urlUnsafeTitle($post))->firstOrFail();
    }


    public function showForm()
    {
        return view('admin.image.upload');
    }

}
