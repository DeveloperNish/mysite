<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ImageController extends Controller
{
    public function uploadImage()
    {
        $input = Input::all();
 
        $rules = array(
            'file' => 'image|max:20000',
        );
 
        $validation = Validator::make($input, $rules);
 
        if ($validation->fails()) {
            return Response::make($validation->errors->first(), 400);
        }
        $timestamp = Carbon::now();
        $destinationPath = 'uploads'; // upload path
        $destinationPath .= '/'.$timestamp->year;
        $destinationPath .= '/'.$timestamp->month;
        $destinationPath .= '/'.$timestamp->day;

        $extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
        $fileName = rand(11111, 99999); // renaming image
        $upload_success = Input::file('file')->move($destinationPath, $fileName. '.' . $extension); // uploading file to given path
        
        if ($upload_success) {
            $image = new Image();
            $image->name = $fileName;
            $image->extension = $extension;
            $image->path = $destinationPath;
            $image->save();
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }
    }

    public function getImage($id){
    	$path = public_path().'/';
    	$image = Image::find($id);
    	if($image){
    		$path .= $image->path.'/';
    		$path .= $image->name.'.'.$image->extension;
    		$file = File::get($path);
    		$type = File::mimeType($path);
    		$response = Response::make($file,200);
    		$response->header("Content-Type",$type);
    		return $response;
    	}
    	else
    	{
    		return \App::abort(404);
    	}
    }

    public function getAllImages(Request $request){
    	if($request->ajax())
    	{
	    	return Image::all('id');
   		}
   		else{
   			return \App::abort(404);
   		}
   }
}
