<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;

class ArticleController extends Controller
{
    public function show($page)
    {
    	$page = Article::urlUnsafeTitle($page);
    	$article = Article::where('title','=',$page)->firstOrFail();

    	return view('public.show',compact('article'));
    }
}
