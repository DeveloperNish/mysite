<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;

class PageController extends Controller
{
    private $paginate = 10;

    public function index()
    {
    	$articles = Article::latest()->paginate($this->paginate);
    	return view('public.index',compact('articles'));
    }

    public function about()
    {
    	return view('public.about');
    }

    public function contact()
    {
    	return view('public.contact');
    }

}