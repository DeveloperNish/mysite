<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Tag;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer("public.nav.fixed-left",function($view){
            $view->with('tags',Tag::all());
        });

        view()->composer('admin.post.create',function($view){
            $view->with('tags',Tag::all());
        });

        view()->composer('admin.post.edit',function($view){
            $view->with('tags',Tag::all());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
