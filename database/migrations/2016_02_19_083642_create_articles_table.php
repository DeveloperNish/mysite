<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("articles", function(Blueprint $table){
            $table->increments("id");
            $table->date("publish_date");
            $table->string("title")->unique();
            $table->string("excerpt");
            $table->string("display_image");
            $table->longtext("content");
            $table->integer("tag_id")->unsigned();
            $table->integer("user_id")->unsigned();
            $table->timestamps();
            $table->foreign("tag_id")
                    ->references("id")->on("tags")
                    ->onDelete("cascade");
            $table->foreign("user_id")
                    ->references("id")->on("users")
                    ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("articles");
    }
}
