/**
 * trigger handler for drop down menu
 */
var drop_menu = document.getElementsByClassName("drop-menu-title");
var dropped = false;
drop_menu[0].onclick = function(){ 
    var drop_menu_items = document.getElementsByClassName("drop-menu-items");
    if(dropped == false)
    {
        dropped = true;
        drop_menu_items[0].style.display = "block";
    }else{
        dropped = false;
        drop_menu_items[0].style.display = "none";
    }
}