$(document).ready(function() {
	var stickyContentMenu = $('.content-menu').offset().top-50;
	var stickyNav = function(){
		var scrollTop = $(window).scrollTop();
		      
		if (scrollTop > stickyContentMenu) { 
			$('.content-menu').css("transition","none");
		    $('.content-menu').addClass('sticky');
		} else {
		    $('.content-menu').removeClass('sticky'); 
		}
	};
	stickyNav();
	$(window).scroll(function() {
	    stickyNav();
	}); 
	autosize($('textarea'));
});