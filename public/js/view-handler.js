var xcsrf = $('meta[name=X-CSRF-TOKEN]').attr("content");
var url = "http://"+window.location.host+"/admin/post/view/"+xcsrf;
var myForm = $('form[name="dataForm"]');
var action = myForm.attr('action');

$('button.view').click(function(){
	myForm.attr('target','_blank');
	myForm.attr('action',url);
	myForm.submit();
	myForm.attr('action',action);
	myForm.removeAttr('target');
});