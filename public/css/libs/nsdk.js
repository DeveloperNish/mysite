var nsdk = nsdk || {

	//readonly data
	data : Object.create(Object.prototype,{
		_id : {writable: false, enumerable: true, value: "#"},
		_class : {writable: false, enumerable: true, value: "."},
		_baseUrl : {writable: false, enumerable: true, configurable: true, value: window.location.host},
	}),

	/**
	 * Utility functions
	 * @type {Object}
	 */
	util : {
		urlSafe : function(str){
			str = str.replace(/\s/g,"-");
			var regexp = /^[a-zA-Z0-9-]+$/;
			if(regexp.test(str)){
				return str;
			}else{
				return false;
			}
		},
	},
	/**
	 * Returns a element node list
	 * @param  {String} elem [name of the element with prefix of type]
	 * @return {NodeList}      returns a dom node list of the type
	 */
	get : function(elem){
		var type = elem.charAt(0);
		var selector = null;
		if(type === nsdk.data._id){
			var elem = elem.substr(1,elem.length);
			selector = document.getElementById(elem);
			selector._type = nsdk.data._id;
		}else if(type === nsdk.data._class){
			var elem = elem.substr(1,elem.length);
			selector = document.getElementsByClassName(elem);
			selector._type = nsdk.data._class;
		}else{
			selector = document.getElementsByName(elem);
			if(selector.length === 0){
				selector = document.getElementsByTagName(elem);
			}
		}
		if(selector.length != 0){
			selector.on = function(trigger,func){
				if(this._type != nsdk.data._id){
					for(var i=0;i<this.length;i++){
						this[i].addEventListener(trigger,func);
					}
				}else{
					this.addEventListener(trigger,func);
				}
			}

			selector.click = function(func){
				this.on("click",func);
			}
		}
		return selector;
	},
	/**
	 * Trigger on window load
	 * @param  {Function} func [a function]
	 * @return {window.onload}      [return the event]
	 */
	load : function(func){
		return window.onload = func;
	},

	/**
	 * Attaches an even tto an element
	 * @param  {String} trigger [trigger type]
	 * @param  {Node||NodeList} elem    [A DOM node of nodelist]
	 * @param  {Function} func    [a function]
	 * @return {NONE}
	 */
	on : function(trigger,elem,func){
		if(elem._type != nsdk.data._id){
			for(var i=0;i<elem.length;i++){
				elem[i].addEventListener(trigger,func);
			}
		}
		else
		{
			elem.addEventListener(trigger,func);
		}
	},
	keydown : function(elem,func){
		nsdk.on("keydown",elem,func);
	},
	keyup : function(elem,func){
		nsdk.on("keyup",elem,func);
	},
	click : function(elem,func){
		nsdk.on("click",elem,func);
	},

	ajax : function(params){
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function(){
			if(xmlhttp.readyState == params["state"] && xhttp.status == params["status"]){
				params["done"];
			}else{
				if ("error" in params){
					params["error"];
				}else{
					alert("ERROR");
				}
			}
		};
		xmlHttp.open(params["method"],params["url"],params["anon"]);
		xmlHttp.send();
	}
};

var ns = nsdk;

/*ns.ajax([
	"method"	=>	"GET",
	"url"		=>	
]);*/